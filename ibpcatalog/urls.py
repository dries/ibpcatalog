from django.conf.urls import patterns, url, include
from views import FeedList, FeedDetail

from models import Feed

urlpatterns = patterns('',
    url(r'^$', FeedList.as_view(), name="feed_list"),
    url(r'^(?P<slug>[\w-]+)/', FeedDetail.as_view(), name="feed_detail")
)