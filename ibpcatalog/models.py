from django.db import models
from ibpcatalog.utils import IbpParser
from treebeard.al_tree import AL_Node

class Node(AL_Node):
    """docstring for Node"""
    
    feed = models.ForeignKey('Feed', related_name='nodes')
    parent = models.ForeignKey('self',
                           related_name='children_set',
                           null=True,
                           db_index=True)
    name = models.CharField(max_length=255)
    calculatedurl = models.URLField(null=True)
    node_order_by = ['name', ]

    def __unicode__(self):
        return self.name

def parse_and_save(feed):
    """
    Function to parse a given feed and save it to the Node model.
    """
    url=feed.url
    print "Parsing", url
    ibpparser = IbpParser(url)
    ibpparser.parse()

    print "Number of entries: ",len(ibpparser.results)
    for result in ibpparser.results:
        
        companynode, created = Node.objects.get_or_create(name=result['companyname'], feed=feed)
        languagenode, created = Node.objects.get_or_create(name=result['language'], parent=companynode, feed=feed)
        productnamenode, created = Node.objects.get_or_create(name=result['productname'], parent=languagenode, feed=feed)
        titlenode, created = Node.objects.get_or_create(name=result['title'], calculatedurl=result['calculatedurl'], parent=productnamenode, feed=feed)
        
        if created:
            print titlenode, "saved..."
        else:
            print titlenode, "already in the database..."

class Feed(models.Model):
    """
    Feed Model: you can have multiple urls each with their own nodes.
    """
    
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    url = models.CharField(
        max_length=1024,
        help_text="Copy and paste your url from <a href='http://cat.internetbrokerproject.be/IBPCatalog/'>here</a>"
    )

    
    def __unicode__(self):
        return self.name
    
    @models.permalink
    def get_absolute_url(self):
        return ('feed_detail', [str(self.slug)])

    def save(self, *args, **kwargs):
        
        super(Feed, self).save(*args, **kwargs) # Call the "real" save() method.
        parse_and_save(self)
        


    