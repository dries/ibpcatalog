from bs4 import BeautifulSoup
import urllib2


class IbpParser(object):
    """docstring for IpbParser"""

    def __init__(self, url):
        self.page = urllib2.urlopen(url).read()
        self.soup = BeautifulSoup(self.page, 'lxml')
        

    def parse(self):
        """
        list van dicts
        dict : keys title,calc_url, author, url_id
        via self.soup

        self.results
        """
        # import ipdb; ipdb.set_trace()
        self.results=[]

        for entry in self.soup.find_all('entry'):
            self.results.append({
                'companyname': unicode(entry.find('d:companyname').string),
                'language': unicode(entry.find('d:language').string),
                'productname': unicode(entry.find('d:productname').string),
                'title': unicode(entry.title.string),
                'calculatedurl': unicode(entry.find('d:calculatedurl').string),

            })

def returnfirstobject(qs):
    r = list(qs[:1])
    if r:
        return r[0]
    return None