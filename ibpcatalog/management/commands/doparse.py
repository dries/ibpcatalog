from django.core.management.base import BaseCommand, CommandError
from ibpcatalog.utils import IbpParser
from ibpcatalog.models import Feed, parse_and_save

class Command(BaseCommand):


    def handle(self, *args, **options):

    	print args
    	for feedname in args:
    		feed = Feed.objects.get(name=feedname)
    		parse_and_save(feed)