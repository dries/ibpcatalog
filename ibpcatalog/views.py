from django.views.generic import ListView, DetailView
from models import Feed
from utils import returnfirstobject



class FeedList(ListView):
    model = Feed

class FeedDetail(DetailView):
    """
    I want to get this:
    <ul>
        <li>
            company1
            <ul>
                <li>
                    productname1
                    <ul>
                        <li>document1</li>
                        <li>document2</li>
                    </ul>
                </li>
                <li>
                    productname2
                    <ul>
                        <li>document1</li>
                    </ul>
                </li>
            </ul>
        </li>


        <li>
            company2
        </li>
    </ul>

    """
    model = Feed
    context_object_name = 'feed'
        
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(FeedDetail, self).get_context_data(**kwargs)
        # Add in extra context
        nodelist = []
        # from ipdb import set_trace; set_trace()
        for parentlessnode in self.object.nodes.filter(parent=None):
            nodelist = nodelist + parentlessnode.get_annotated_list(parent=parentlessnode)
        context['nodelist'] = nodelist
        
        
        return context