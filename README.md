
# Ibpcatalog Django App #

## About ##

Describe ibpcatalog here.

## How to install the app ##

* Simply run 'python setup.py install'

## How to run the example project ##

* cd into the 'example' folder
* run 'python manage.py syncdb' and fill in the superuser details
* run 'python manage.py runserver'